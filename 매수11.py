
        elif self.db_to_realtime_daily_buy_list_num == 11:
            if i < 2:
                realtime_daily_buy_list = []
                pass
            the_day_before_yesterday = self.date_rows[i-2][0]
            # date_rows_yesterday는 실제 트레이딩하는 날 기준 하루 전 일자를 의미하므로 20201028이 담깁니다.  
            # date_rows_yesterday는 self.date_rows[i-1][0] 값이 담겨 있습니다. 
            # the_day_before_yesterday 는 실제 트레이딩하는 날 기준 이틀 전 일자를 의마하므로 20201027이 담깁니다. 
            # 아래 쿼리는 기존 쿼리 보다 보기가 편하도록 f-string을 사용했습니다. 
            sql = f'''
                select a.* from `{date_rows_yesterday}` a where a.d1_diff_rate > 5 
                and exists (select null from `{the_day_before_yesterday}` b where b.code = a.code 
                and b.d1_diff_rate < 3)
                and NOT exists (select null from stock_konex c where a.code=c.code) 
                and a.close < {self.invest_unit} group by code 
            '''
            # 아래 명령을 통해 테이블로 부터 데이터를 가져오면 리스트 형태로 realtime_daily_buy_list 에 담긴다.
            realtime_daily_buy_list = self.engine_daily_buy_list.execute(sql).fetchall()

